Respostas ao exercício preparatório do experimento 2 - EE641
============================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142

.. figure:: img/sit1.png
   :scale: 95%

   Situação 1 (0.5, 0.5, 0.5)

Na situação 1, em que os três potenciômetros estão em 0.5, temos uma resposta
praticamente plana para o módulo do equalizador na faixa de 20Hz a 20kHz, com
um pouco de atenuação para frequências abaixo de 1kHz.

A fase é praticamente constante em +-180 graus, ou seja o sinal é invertido na
saída,  na faixa de interesse, entre 20Hz e 20kHz já que se trata de uma
aplicação de áudio.

.. figure:: img/sit2.png
   :scale: 95%

   Comparação entre a situação 2 (1, 0.5, 0.5) e a situação 1

Com o aumento do potenciômetro de altas frequências para 1, há grande atenuação
de altas frequências, com queda a partir de 1kHz e atingindo atenuação de 20dB
para 20kHz.
A frequência de corte (-3dB) é em torno de 1.5kHz.
Além disso, há uma pequena ressonância em torno de 500Hz.

Já para a fase, ocorre uma oscilação a partir de 200Hz em torno de 150 graus.

.. figure:: img/sit3.png
   :scale: 95%

   Comparação entre a situação 3 (0.5, 1, 0.5) e a situação 1

A mudança do potenciômetro de médias frequências afeta praticamente toda a
região de interesse, porém a atenuação é mais acentuada (maior que 3dB) entre
200Hz e 15kHz, e máxima em torno de 2.5kHz, atenuando 12.5dB do sinal.

A fase flutua ao longo de toda região de interesse em torno de 180 graus
chegando a adiantar e atrasar 30 graus.

.. figure:: img/sit4.png
   :scale: 95%

   Comparação entre a situação 4 (0.5, 0.5, 1) e a situação 1

Aumentando-se o potenciômetro de baixas frequências para 1 ocorre a atenuação
das baixas frequências, abaixo de 1kHz.
Em 20Hz há uma atenuação de aproximadamente 18dB.
A frequência de corte (-3dB) situa-se em torno de 600Hz

A fase é impactada abaixo de 7kHz, oscilando em torno de -150 graus.

.. figure:: img/sit5.png
   :scale: 95%

   Comparação entre a situação 5 (0, 0.5, 0.5) e a situação 1

Com a diminuição do potenciômetro de altas frequências para 0, há amplificação
do sinal em altas frequências, principalmente em torno de 40kHz.
O sinal é amplificado a partir de 1kHz, com leve atenuação de frequências logo
abaixo.

A fase é impactada ao longo da região de interesse e oscila em torno de -150
graus.

.. figure:: img/sit6.png
   :scale: 95%

   Comparação entre a situação 6 (0.5, 0, 0.5) e a situação 1

A diminuição do potenciômetro de médias frequências amplifica o sinal em
praticamente toda a região de interesse, mas principalmente em torno de 2.5Hz,
onde é amplificado em 13dB.

A fase também é impactada e oscila em torno de 180 graus.

.. figure:: img/sit7.png
   :scale: 95%

   Comparação entre a situação 7 (0.5, 0.5, 0) e a situação 1

Por fim, com o potenciômetro de baixas frequências em 0, as baixas frequências
são amplificadas, abaixo de 1kHz e com pico em 30Hz, com ganho de 16dB.

A fase oscila em torno de 150 graus.

Conclusão
---------

Finalmente, pode-se concluir que cada um dos potenciômetros impacta
principalmente no ganho ou atenuação de sua faixa de frequências.
Valores acima de 0.5 causam atenuação e valores abaixo de 0.5, amplificação do
sinal naquela faixa.

O potenciômetro de médias frequências é o que possui impacto mais expressivo ao
longo da faixa de áudio, com os potenciômetros de baixa e alta frequências sendo
mais expressivos apenas nas extremidades.

Além disso, o desvio dos potenciômetros de 0.5 causa pequenas oscilações da fase
ao longo da faixa de interesse.
