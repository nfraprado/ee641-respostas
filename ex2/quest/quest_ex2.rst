Respostas ao questionário do experimento 2 - EE641
==================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142

1. Em cada uma das 07(sete) condições mostradas nas figuras disponibilizadas, compare e explique as medidas realizadas com aquelas das simulações
-------------------------------------------------------------------------------------------------------------------------------------------------

Antes de mais nada, houve a inversão do valor de SET para os potenciômetros do
circuito experimental em relação ao simulado (e utilizado no pré-relatório), mas
para a comparação aqui será utilizada a convenção usada na etapa experimental.

.. figure:: img/sit1_cmp.png
   :width: 100%

   Comparação entre a situação 1 (0.5, 0.5, 0.5) simulada e a experimental

Essa configuração corresponde à saída equalizada.

A resposta simulada é praticamente plana e de ganho unitário, enquanto a
experimental possui um pequeno ganho em baixas e altas frequências e uma leve
atenuação em médias frequências, indicando que o potenciômetro provavelmente não
se encontrava exatamente na metade para os três casos: P1 e P3 estavam levemente
acima da metade e P2 levemente abaixo.

.. figure:: img/sit2_cmp.png
   :width: 100%

   Comparação entre a situação 2 (1, 0.5, 0.5) simulada e a experimental

Essa configuração corresponde à amplificação de altas frequências.

A forma das curvas é bastante parecida, com a única maior diferença sendo o pico
de amplificação em 40kHz no circuito simulado enquanto este foi por volta de
70kHz no experimental.

.. figure:: img/sit3_cmp.png
   :width: 100%

   Comparação entre a situação 3 (0.5, 1, 0.5) simulada e a experimental

Essa configuração corresponde à amplificação de médias frequências.

As curvas são bastante similares, com pico de amplificação concidindo também.
Uma pequena diferença é que a curva simulada decai mais rapidamente para altas
frequências do que para baixas, mas ambas são suaves.
Já no caso experimental, também cai mais devagar para baixas frequências porém
há um "plateau" de 100 a 400 Hz.

.. figure:: img/sit4_cmp.png
   :width: 100%

   Comparação entre a situação 4 (0.5, 0.5, 1) simulada e a experimental

Essa configuração corresponde à amplificação de baixas frequências.

Para este caso, por mais que dentro da faixa de interesse as curvas sejam muito
semelhantes, é possível perceber que na simulação o pico se deu em 30Hz,
enquanto no experimental o pico se encontra abaixo de 10 Hz.

.. figure:: img/sit5_cmp.png
   :width: 100%

   Comparação entre a situação 5 (0, 0.5, 0.5) simulada e a experimental

Essa configuração corresponde à atenuação de altas frequências.

O perfil das curvas é bem próximo, principalmente para altas frequências em que
o simulado tem uma atenuação de 20dB em 20kHz enquanto no experimental, em 20kHz
o ganho é bem próximo de 0.1.
A maior diferença nesse caso se dá nas baixas frequências, em que o ganho
deveria ser unitário, sendo que há amplificação no experimental, mas mais uma
vez isso deve ter ocorrido por conta da imprecisão do potenciômetro, que deveria
estar ligeiramente acima da metade.

.. figure:: img/sit6_cmp.png
   :width: 100%

   Comparação entre a situação 6 (0.5, 0, 0.5) simulada e a experimental

Essa configuração corresponde à atenuação de médias frequências.

Mais uma vez as maiores diferenças provavelmente se deram por imprecisão do
potenciômetro, como a amplificação das baixas frequências, sendo que idealmente
deveria haveer inclusive uma atenuação das baixas frequências em relação às
altas.

.. figure:: img/sit7_cmp.png
   :width: 100%

   Comparação entre a situação 7 (0.5, 0.5, 0) simulada e a experimental

Essa configuração corresponde à atenuação de baixas frequências.

O perfil da curva experimental é novamente próximo ao simulado, com atenuação
similar nas baixas frequências, e o ganho em altas frequências deve ser por
conta do potenciômetro P1 levemente acima da metade.

2. Existe uma das medidas onde deve-se observar a limitação imposta pelo "slew rate" do opamp, caso contrário, poderemos obter uma resposta em frequência errada, indique qual medida e explique esta limitação
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A medida em que se deve respeitar a limitação do "slew rate" é a 2, em que há
amplificação das altas frequências.
Isso porque o parâmetro de "slew rate" indica a máxima taxa de variação da
tensão do sinal na saída do dispositivo.

Dessa forma, mantendo-se a amplitude na saída constante, existe uma máxima
frequência de operação permitida para o dispositivo, antes que a saída deixe de
conseguir acompanhar a entrada.
Ou então, mantendo-se a frequência do sinal de saída constante, existe uma
máxima amplitude do sinal permitida.


3. Encontre o máximo valor da amplitude do sinal de entrada para que a situação acima não ocorra
------------------------------------------------------------------------------------------------

O datasheet do RC4580 caracteriza seu valor de "slew rate" como sendo de 5V/us.

Como se trata de uma aplicação de áudio, a frequência de interesse mais alta é
20kHz.

Uma frequência de 20kHz corresponde a um período de 50us.
Portanto em 1us, apenas 1/50 do período do sinal passou, e portanto
aproximadamente (aproximando a senóide a uma onda dente de serra) 1/50 da sua
amplitude também.

Dessa forma, para atingir o limite de 5V/us a 20kHz, é necessária uma amplitude
de 5 * 50 = 250V na saída.
Considerando o ganho medido de aproximadamente 10V/V para 20kHz, a tensão de
entrada que atingiria o limite do slew rate do amplificador operacional seria de
25Vpp.

Tanto o valor de tensão de entrada quando de saída calculados estão acima dos
permitidos para o dispositivo, o que demonstra que para sua faixa de operação em
frequência, o slew rate não é um problema, o que demonstra a aplicabilidade do
componente a aplicações de áudio.
