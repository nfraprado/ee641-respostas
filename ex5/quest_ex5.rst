Respostas ao questionário do experimento 5 - EE641
==================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142

1. Explique o comportamento da resposta em frequência desse amplificador.
-------------------------------------------------------------------------

.. image:: img/resp_freq.bmp
   :width: 100%

O corte para baixas frequências, abaixo de 40Hz, se deve ao filtro RC passa-alta
que se encontra na entrada do amplificador.

Para frequências muito altas, a associação série RC na realimentação do amp op
diminui o ganho.

Além disso há um ponto de ressonância em torno de 400kHz possivelmente devido à
associação RLC em paralelo com o alto-falante na saída do amp op.

2. Esboce os dois amplificadores de áudio para explicar o uso do modo "Bridge"
------------------------------------------------------------------------------

.. image:: img/esboco_bridge.png
   :width: 100%

3. Deduza a expressão do ganho das duas configurações.
------------------------------------------------------

O ganho do amp op na configuração inversor é:

.. math:: G_{inv} = -\frac{R2}{R1} = -\frac{47k1}{5k6} = -8.41

E portanto o ganho do lado direito:

.. math:: G_R = G_{inv} = -8.41

Já o ganho do amplificador não-inversor é:

.. math:: G_{n\_inv} = 1 + \frac{R2}{R1} = 1 + \frac{47k1}{5k6} = 9.41

E o ganho devido ao divisor de tensão:

.. math:: G_{div} = \frac{R2}{R1 + R2} = \frac{47k1}{47k1 + 5k6} = 0.89

Portanto o ganho do lado esquerdo é:

.. math:: G_L = G_{div} \times G_{n\_inv} = 8.37

Finalmente, o ganho do modo *bridge*, que junta os sinais de ambos os lados é:

.. math:: G_{bridge} = G_L - -G_R = G_L + G_R = 8.37 + 8.41 = 16.78 \approx 2G_L
