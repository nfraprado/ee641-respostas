Respostas ao questionário do experimento 4 - EE641
==================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142

1. Qual a função deste amplificador?
------------------------------------

A função deste amplificador é atuar como um compressor dinâmico de sinal, isto
é, ajustar o ganho de forma a maximizá-lo porém sem saturar o amp op, o que
causaria distorções muito perceptíveis.
Para isso o amplificador deve reduzir o ganho conforme a amplitude do sinal de
entrada aumenta, garantindo assim que a condição de saturação não seja atingida.

2. Qual a função do detector de pico?
-------------------------------------

A função do detector de pico é fornecer um nível de tensão que varia
proporcionalmente com a amplitude do sinal de entrada, para que possa ser
utilizado para controlar o ganho do amplificador através da variação da
resistência equivalente do JFET.
Dessa forma o ganho do amplificador pode ser inversamente proporcional ao pico
de tensão da entrada.


3. Observe as figuras das medidas realizadas e descreva o comportamento dos circuitos.
--------------------------------------------------------------------------------------

.. figure:: img/tran_03.jpg
   :width: 100%

   Análise transiente da entrada e saída para Vref = 0.3V

Para um valor de tensão de referência próximo de 0, o ganho do amplificador é
máximo, e aproximadamente 3.
Isso porque o canal do JFET não é estrangulado e portanto sua resistência
equivalente é baixa, tornando o ganho do amp op alto.

Como a tensão de referência corresponde à tensão de pico do semiciclo negativo
da entrada, esse caso de ganho máximo ocorre na situação em que a entrada tem
amplitude muito baixa, praticamente nula.

.. figure:: img/tran_-3.jpg
   :width: 100%

   Análise transiente da entrada e saída para Vref = -3V

.. figure:: img/tran_-6.jpg
   :width: 100%

   Análise transiente da entrada e saída para Vref = -6V

.. figure:: img/tran_-9.jpg
   :width: 100%

   Análise transiente da entrada e saída para Vref = -9V

Conforme a tensão de limiar vai ficando mais negativa, correspondendo a
amplitudes cada vez maiores do sinal de entrada, o ganho vai diminuindo, até que
para Vref = -9, o ganho no semiciclo positivo é mínimo, por volta de 10%.

Apesar disso, pode-se notar a não-linearidade do JFET, que se comporta
diferentemente para tensões de mesma amplitude porém polaridade oposta.
Isso fica evidente para Vref mais negativo como -9, em que o semiciclo negativo
da saída é muito mais alongado do que o positivo.
Esse fenômeno representa distorção do sinal de saída, que, no entanto, é mais
desejável do que a saturação.

.. figure:: img/tran_jumper.jpg
   :width: 100%

   Análise transiente da entrada e saída com compressor dinâmico em curto

Com o compressor dinâmico em curto é possível avaliar o desempenho da saída
caso não fosse utilizado esse circuito.
O resultado é, para altas amplitudes de entrada, uma saída saturada,
representando péssima qualidade sonora.

.. figure:: img/ac_ganho.bmp
   :width: 100%

   Análise AC: Ganho

.. figure:: img/ac_vpp.bmp
   :width: 100%

   Análise AC: Tensão pico a pico da saída

Os gráficos da análise AC permitem confirmar que com os valores escolhidos para
os componentes do circuito, a performance do circuito é satisfatória, dada a
resposta em frequência praticamente plana ao longo da faixa de interesse: 20Hz a
20kHz.
