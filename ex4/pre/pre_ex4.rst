Respostas ao exercício preparatório do experimento 4 - EE641
============================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142

.. figure:: img/tran_out.png
   :width: 100%

   Análise transiente da tensão de saída em relação à tensão de entrada para
   diferentes tensões Vfet

Fica claro com base no gráfico que quanto mais negativa a tensão Vfet, menos
ganho o sinal de saída possui.
Uma tensão de -10V torna o ganho próximo de 0.2.
Tensões ainda mais negativas foram testadas, porém com pouca diferença.

Já uma tensão de 0V proporciona um ganho de 3 em relação à entrada.
Tensões positivas também foram testadas porém apresentaram o mesmo comportamento
de 0V.

É possível notar que essa dependência não é linear, já que a variação da
amplitude é maior entre -5 e -6 do que entre 0 e -5 e do que entre -6 e -10.

Além disso é possível reparar que ocorre uma defasagem mínima dos sinais de
saída em relação ao sinal de entrada.

.. figure:: img/ac_out.png
   :width: 100%

   Análise AC (pequenos sinais) da tensão de saída em relação à tensão de
   entrada para diferentes tensões Vfet

A análise AC deixa mais clara a dependência não linear entre Vfet e o ganho, já
que novamente a maior variação do ganho ocorre para Vfet entre -5 e -6.

É possível reparar que baixas-frequências são atenuadas, devido ao filtro
passa-alta na entrada do circuito, com frequência de corte próxima de 10Hz.

O gráfico de fase não mostra nenhuma variação, no entanto, indicando que a
alteração na fase na simulação anterior pode ter sido apenas um fenômeno
transitório ou erro na simulação.
