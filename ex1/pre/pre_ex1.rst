Respostas ao exercício preparatório do experimento 1 - EE641
============================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142

Circuito 1
----------

.. figure:: img/cir1-set00.png
   :scale: 80%

   Resposta em frequência para SET = 0

.. figure:: img/cir1-set05.png
   :scale: 80%

   Resposta em frequência para SET = 0.5

.. figure:: img/cir1-set10.png
   :scale: 90%

   Resposta em frequência para SET = 1

Quando SET = 0, a saída do circuito é atenuada em relação à entrada, da ordem
de 20dB.
Já quando SET = 0.5, a saída é igual à entrada.
Finalmente, quando SET = 1, a saída é amplificada em relação à entrada, na
ordem de 20dB.

Portanto pode-se concluir que o circuito apenas funciona de fato como
amplificador para SET >= 0.5.
Além disso, o ganho e defasagem são aproximadamente constantes na faixa de
10Hz a 10 kHz.
Acima desse intervalo há diminuição gradual do ganho e aumento gradual da
defasagem.

Circuito 2
----------

.. figure:: img/cir2-tran.png
   :scale: 95%

   Análise transiente de Ve

Considerando uma entrada AC de 2V de amplitude e a 50Hz é possível perceber
que os diodos apenas conduzem durante os picos positivos da tensão de entrada,
funcionando portanto como um detector de nível.

.. figure:: img/cir2-dc.png
   :scale: 95%

   Análise de varredura DC em Ve

Através da análise de varredura em DC, percebe-se que os diodos começam a
conduzir quando a tensão de entrada é em torno de 2V.
Assim, conclui-se que o circuito se comporta como um detector de nível com
tensão de limiar 2V, detectando sempre que a entrada ultrapassa esse limite.
