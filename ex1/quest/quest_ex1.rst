Respostas ao questionário do experimento 1 - EE641
==================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142


Circuito 1 - Pré-amplificador
-----------------------------

Questão 1
~~~~~~~~~

Divindo-se o circuito em duas malhas, uma da entrada para o terra, e outra da
saída para o terra, é possível obter as seguintes relações por divisor de
tensão:

.. math::

   \frac{V_{pot}}{V_i} = \frac{50k(s) + 3.3k}{50k(s) + 3.3k + 30k}

.. math::

   \frac{V_{pot}}{V_o} = \frac{50k (1-s) + 3.3k}{50k (1-s) + 3.3k + 30k}

Como ambas aprensentam a tensão do potenciômetro, é possível utilizar esse fator
em comum para obter a expressão de ganho em função de s:

.. math::

   \frac{V_o}{V_i} = \frac{50k(s) + 3.3k}{50k(s) + 3.3k + 30k}\frac{50k(1-s) + 3.3k + 30k}{50k(1 - s) + 3.3k}

que após distribuição dos produtos e arrendondamento dos valores fornece a
seguinte expressão:

.. math::

   \frac{V_o}{V_i} \approx \frac{50(1-s) + 333s + 33(1-s) + 22}{50(1-s) + 333(1-s) + 33(s) + 22}

Que pode ser validada para os casos limites:

.. math::

   s = 0: \frac{V_o}{V_i} \approx 0.1

.. math::

   s = 1: \frac{V_o}{V_i} \approx 10

Questão 2
~~~~~~~~~

Para as três posições do potenciômetro, percebe-se que há bastante proximidade
entre os valores simulados e os medidos.
Enquanto a resposta simulada é plana na faixa de interesse, a medida não é, por
não ser ideal, porém é bastante próxima.
Praticamente constante.

A posição que apresenta maior divergência é a de SET = 0.5, que apresenta erro
de aproximadamente 10% a mais do ganho unitário que era esperado, o que pode ser
justificado pela imperfeição do potenciômetro, cuja posição central não divide
ambos os lados com resistência exatamente igual.

A não ser por isso, as medidas foram bastante próximas do simulado, com SET = 0
fornecendo ganho 1/10 (ou -20dB), SET = 0.5 fornecendo ganho unitário (0 dB), e
SET = 1 fornecendo ganho 10 (20dB).

Circuito 2 - Detector de pico
-----------------------------

Questão 1
~~~~~~~~~

A principal diferença que pode-se perceber é a diferença no ponto de inflexão
das curvas em relação à tensão de entrada.
Enquanto na simulação ele era em torno de 2V, nas medidas experimentais ocorreu
em 600mV.
A razão disso se deve principalmente à maneira como foi realizada a medição, que 
utilizou ambos os canais para acoplar o sinal, enquanto que na simulação foi
utilizado apenas um canal, com o outro aterrado.
Dessa forma, a medida experimental precisaria de metade da tensão na entrada
para atingir o mesmo ponto da simulação (mas foi obtido 600mV ao invés do 1V que
seria esperado, talvez por conta de imperfeições também).

Além disso há algumas diferenças também na tensão do coletor de Q2, talvez
derivado de imperfeições dos modelos SPICE.

De toda forma o funcionamento do circuito é similar.

Questão 2
~~~~~~~~~

O perfil das curvas simuladas e as medidas são similares, com uma diferença
marcante que é a tensão na base do transistor é senoidal, com pico em sua tensão
Vbe, ao invés de ter o achatamento característico da medição.
Além disso, a condução do LED é bem efêmera, não dando tanta oportunidade para o
capacitor extendê-la e evitar piscar do LED, que não é desejado.

Ambos os problemas podem ser justificados pela simulação do circuito ter sido
realizada com uma fonte de pico 2V, logo na tensão de limiar, dando pouca
oportunidade para o circuito conduzir.
Portanto a simulação evidencia bem o perfil de detecção de pico do circuito,
porém não demonstra o seu funcionamento ideal.
