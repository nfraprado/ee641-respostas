Respostas ao exercício preparatório do experimento 3 - EE641
============================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142

.. figure:: img/bargraph.png
   :width: 100%

   Comportamento do bargraft para um DC sweep

A figura mostra a tensão de saída sobre cada uma das cargas em função da tensão
de entrada.

O circuito consiste basicamente de divisores de tensão cascateados, onde cada
nível de tensão é comparado com o sinal de entrada, levando a saída para nível
alto quando a entrada ultrapassa esse nível de tensão.
Dessa forma, as saídas com nível alto indicam os níveis de tensão dos quais a
entrada ultrapassou.

O gráfico evidencia esse comportamento, com o nível de tensão de cada saída
transicionando para alto quando a entrada ultrapassa da tensão estabelecida por
seu divisor de tensão.

As seguintes figuras mostram as transições ampliadas e o valor da tensão
correspondente no eixo x:

.. figure:: img/bargraph_wlines_0_4.png
   :width: 100%

   Tensões de transição para as saídas de 1 a 4

.. figure:: img/bargraph_wlines_4_8.png
   :width: 100%

   Tensões de transição para as saídas de 5 a 8

.. figure:: img/bargraph_wlines_8_12.png
   :width: 100%

   Tensões de transição para as saídas de 9 a 12

Admitindo-se 1.6V como a referência (0dB) é possível converter os valores
de tensão obtidos nos gráficos anteriores para dB, através da seguinte fórmula:

.. math:: V_{dB} = 20\log_{10}\left(\frac{V_{V}}{1.6}\right)

Os resultados são:

=============== ============ ===========
Tensão em Volts Tensão em dB dB desejado
=============== ============ ===========
0.025V          -36.12dB     -30dB
0.125V          -22.14dB     -20dB
0.45V           -11.01dB     -10dB
0.7V            -7.18dB      -7dB
1.05V           -3.66dB      -4dB
1.3V            -1.80dB      -2dB
1.625V          0.13dB       0dB
2V              1.94dB       2dB
2.45V           3.70dB       4dB
3.425V          6.61dB       7dB
4.875V          9.68dB       10dB
8.975V          14.98dB      15dB
=============== ============ ===========

Conclusão
---------

Percebe-se que o circuito simulado funcionou próximo do esperado, com cada uma
de suas cargas sendo alimentada a depender se o nível ultrapassou o limiar em dB
projetado.
