Respostas ao questionário do experimento 3 - EE641
==================================================

.. space:: 250

.. role:: author

.. class:: author

   Aluno: Nícolas F. R. A. Prado

   RA: 185142

1. Explique o funcionamento da fonte de corrente utilizada no Bargraph
----------------------------------------------------------------------

Como a queda de tensão entre emissor e base do TBJ é equivalente à de um diodo,
a queda de tensão sobre o diodo D2 é a mesma que a tensão Vbe em Q1. Assim, a
tensão sobre o resistor R1 será a mesma tensão que cai sobre D1, de
aproximadamente 0.7V.

Assim, o valor de R1 determina o valor de corrente dessa fonte:

.. math:: i = \frac{V}{R} = \frac{0.7}{120} \approx 5mA

2. Comente sobre os valores de tensão mostrada na tabela disponibilizada.
-------------------------------------------------------------------------

Os valores de tensão medidos seguem uma escala logarítmica conforme projetado,
havendo pequenas divergências dos valores em dB esperados, mas que são naturais
devido à incerteza sobre o valor de resistência nominal dos resistores.

3. Comente sobre as formas de onda obtidas no Osciloscópio.
-----------------------------------------------------------

A forma de onda de entrada possui amplitude de cerca de 10V, que é maior que o
limiar de tensão projetado para todos os comparadores, e portanto faz com
que todos os comparadores tenham saída de alta impedância e todos os LEDs sejam
acesos.

Como a entrada é invertida, o acendimento dos LEDs ocorre no semiciclo negativo
da entrada, enquanto no semiciclo positivo todos eles são desligados.

A medida no último LED corresponde apenas à queda de tensão sobre ele, e
portanto no período em que está desligado, a tensão é de -15V, por conta do
dreno de corrente pelo comparador, e no período em que está ligado, deveria ser
de cerca de 0.7V. Apesar disso, a tensão medida é quase de 2V.

A medida no primeiro LED corresponde à queda de tensão de todos os LEDs juntos.
Sendo 12 LEDs,

.. math:: 12 * 0.7 = 8.4

.. math:: -15 + 8.4 = -6.6V

seria a tensão medida considerando queda de 0.7V em cada LED.
Porém a tensão medida é de 10V.

.. math:: 10 - -15 = 25

.. math:: \frac{25}{12} \approx 2V

portanto novamente a tensão vista sobre cada LED é de 2V.

4. Quais as fontes de erro nos pontos de comparação do Bargraph?
----------------------------------------------------------------

Os valores dos resistores apresentam precisão em torno de 5% do valor nominal e
portanto é uma fonte de erro dos valores de comparação de tensão.

Além disso os comparadores podem apresentar tensão de offset, fazendo com que a
tensão de ativação tenha um desvio em relação à tensão projetada no divisor de
tensão.

5. O que limita o número máximo de Leds neste sistema?
------------------------------------------------------

O número de LEDs é limitado pelo valor de tensão de alimentação e pela queda de
tensão sobre cada um.

Como os LEDs, por serem diodos, apresentam uma queda de tensão quando estão
conduzindo, e o arranjo do circuito conecta os LEDs de forma sequencial, é
necessário que a tensão que alimenta os LEDs seja maior do que a queda de tensão
sobre todos eles somadas para que possam conduzir.
