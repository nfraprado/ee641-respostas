Respostas para os roteiros de EE641
===================================

Este repositório contém meus relatórios para os roteiros de EE641 - Laboratório
de Eletrônica II.
Aqui encontra-se minha interpretação dos gráficos obtidos por meio de
simulação SPICE para responder aos questionamentos dos roteiros.

Para a simulação dos circuitos e geração dos gráficos, veja
ee641-software-livre_.

Note que diferentemente de ee641-software-livre_, que é objetivo e portanto mais
confiável (porém fique à vontade para abrir Issues corrigindo erros por lá),
este repositório é totalmente subjetivo e reflete apenas a **minha**
interpretação dos gráficos, e portanto não deve ser usado como referência.

.. _ee641-software-livre: https://gitlab.com/nfraprado/ee641-software-livre
